import { Component, OnInit } from '@angular/core';
import { PrimeIcons } from 'primeng/api';


@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.scss']
})
export class ListBookingComponent implements OnInit {
  events1: any[] = [];

  
  ngOnInit() {
    this.events1 = [
        { status: 'ทันตกรรม', date: '15/10/2020 10:30', icon: PrimeIcons.SHOPPING_CART, color: '#9C27B0', image: 'game-controller.jpg' },
        { status: 'ตรวจทั่วไป', date: '15/10/2020 14:00', icon: PrimeIcons.COG, color: '#673AB7' },
        { status: 'แผนไทย', date: '15/10/2020 16:15', icon: PrimeIcons.ENVELOPE, color: '#FF9800' },
        { status: 'ทันตกรรม', date: '16/10/2020 10:00', icon: PrimeIcons.CHECK, color: '#607D8B' }
    ];

  
}


}
