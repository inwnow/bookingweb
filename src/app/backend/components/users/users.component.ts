import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { UsersService } from './users.service';
import { User } from 'src/app/demo/api/user';
import { an, bo } from '@fullcalendar/core/internal-common';
import { Table } from 'primeng/table';

@Component({
    selector: 'app-backend-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
    blockedPanel: boolean = false;
    // profiles
    user_id: any;
    cid: any;
    fullname: any;
    phone_number: any;
    position: any;
    title: any;
    is_hospital:boolean = true;
    is_service:boolean = false;
    // list hospital
    hospitals: any; 

    // edit profiles
    cidEdit: any;
    fullNameEdit: any;
    hospitalId: any;
    phoneNumberEdit: any;
    posiTionEdit: any;
    tiTleEdit: any;

    // service_id
    selectService: any;
    services: any;

    serviceall: any;

    serviceId: any;

    hospitalBack:any;

    // edit Roles
    selectedDrop: any;
    // roles
    roles: any;
    profiles: any;
    rolesall: any;

    users: any;
    displayForm: boolean = false;

    userName: string = '';
    passWord: string = '';
    usersActive: boolean = true;

    userIdEdit: number = 0;
    userNameEdit: string = '';
    passWordEdit: string = '';
    passWordD: string = '';
    userActiveEdit: boolean = true;

    isAdd: boolean = false;
    isEdit: boolean = false;

    usersStatus = [
        { label: 'Active', value: true },
        { label: 'Inactive', value: false },
    ];

    constructor(
        private layoutService: LayoutService,
        private usersService: UsersService
    ) {}

    async ngOnInit() {
        await this.getHospitalData();
        await this.getRolesAllData();
        await this.getServiceData();
        await this.getData();
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

    // get profiles
    async getProfilesData(id: any) {
        const res: any = await this.usersService.getProfilesById(id);
        let profiles = [];
        profiles = res.data;
        console.log('profiles:', profiles);
        if (profiles.ok) {
            this.profiles = profiles.results[0];
        } else {
            alert('Error loading users');
        }
    }

    // get hospital
    async getHospitalData() {
        let res:any;
        let id: any = sessionStorage.getItem('hospitalId');

        if(id == 1){
            res = await this.usersService.listHospital();      
        }else{
            res = await this.usersService.listHospitalId(id);         
        }
        console.log(res);
        
        let hospital = [];
        hospital = res.data;
        if (hospital.ok) {
            this.hospitals = hospital.results;
            this.hospitalBack = hospital.results;

        } else {
            alert('Error loading users');
        }
        console.log(this.hospitals);
    }

    // get Roles
    async getRolesData(id: any) {
        const res: any = await this.usersService.getRolesById(id);
        this.roles = res.data.results;
        console.log('roles:', this.roles);
    }

    // get RolesAll
    async getRolesAllData() {
        const res: any = await this.usersService.listRoles();
        this.rolesall = [];
        let role1 = [];
        let role2 = [];

        for(let v of res.data.results){
            if(v.role_id == 4){
                
            }else{
                role1.push(v);
            }
        }
        if(sessionStorage.getItem('userRole') == '1'){
            this.rolesall = role1;
        }else{
            for(let v of role1){
                if(v.role_id == 1){}else{this.rolesall.push(v)}
            }
        }
        console.log('roles:', this.rolesall);
        console.log('roles:', this.rolesall);
    }

    // get Service จุดบริการ
    async getServiceData() {
        const res: any = await this.usersService.listService();
        let service = [];
        service = res.data;
        if (service.ok) {
            this.services = service.results;
        } else {
            alert('Error loading users');
        }
        console.log(this.services);
        // this.service = res.data.results;
        // console.log('service:', this.service);
    }

    // get ServiceAll จุดบริการ
    // async getServiceAllData() {
    //     const res: any = await this.usersService.listProfiles();
    //     this.serviceall = res.data.results;
    //     console.log('service:', this.service);
    // }

    // get Services data from API แสดงหน้าแรก
    async getData() {
        this.blockedPanel = true;
        try {
            let id: any = sessionStorage.getItem('hospitalId');
            let res: any;
            if (id == 1) {
                res = await this.usersService.list();
            } else {
                res = await this.usersService.getByHospitalId(id);
            }
            let users_ = [];
            users_ = res.data;

            if (users_.ok) {
                this.users = users_.results;
                for (let u of this.users) {
                    let profile: any = await this.usersService.getProfilesById(
                        u.user_id
                    );
                    console.log("profile.data :",profile.data);
                    
                    if (profile.data.results.length > 0) {
                        u.fullname = profile.data.results[0].fullname;
                        u.position = profile.data.results[0].position;
                        if(profile.data.results[0].hospital_id){
                            let hospital: any = this.hospitals.find(
                                (h: any) =>
                                    h.hospital_id ==
                                    profile.data.results[0].hospital_id
                            );
                            u.hospital_name = hospital.hospital_name;
                        }

                    }
                }
            } else {
                alert('Error loading users');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;

            console.log(error);
        }
        console.log('users:', this.users);
    }

    // display form add data
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.userName = '';
        this.passWord = '';
        this.fullname = null;
        this.position = null;
        this.phone_number = null;
        this.title = null;
        this.cid = null;
    }

    // display form edit data
    async displayFormEdit(data: any) {
        console.log('data', data);
        console.log('rolesall', this.rolesall);

        await this.getRolesData(data.role_id);
        await this.getProfilesData(data.user_id);

        // await this.getServiceData(data.service_id);

        console.log(this.profiles);

        this.isAdd = false;
        this.isEdit = true;
        // edit profile
        this.cidEdit = this.profiles.cid;
        this.fullNameEdit = this.profiles.fullname;
        this.hospitalId = this.profiles.hospital_id;
        this.serviceId = this.profiles.service_id;
        this.phoneNumberEdit = this.profiles.phone_number;
        this.posiTionEdit = this.profiles.position;
        this.tiTleEdit = this.profiles.title;
        this.userIdEdit = data.user_id;
        this.userNameEdit = data.username;
        this.passWordEdit = '';
        // this.passWord = data.password_hash;
        this.userActiveEdit = data.is_active;
        this.displayForm = true;

        for (let v of this.rolesall) {
            if (v.role_id == data.role_id) {
                this.selectedDrop = v;
            }
        }
    }

    async save() {
        this.saveUser();
        // this.saveProfiles();
    }

    async saveProfiles() {
        let service_id:any;
        if(this.selectService){
            service_id = this.selectService.service_id;
        }else{
            service_id = null;
        }
        console.log(this.selectService);
        

        let data = {
            cid: this.cid,
            fullname: this.fullname,
            phone_number: this.phone_number,
            position: this.position,
            title: this.title,
            user_id: this.user_id,
            hospital_id: this.hospitalId,
            service_id: service_id
        };

        console.log("saveProfiles :",data);
        
        // save data profice
        let rs: any = await this.usersService.saveProfiles(data);
        console.log(rs);
    }

    // function save data
    async saveUser() {
        if (this.passWord) {
            let data = {
                username: this.userName,
                password: this.passWord,
                role_id: this.selectedDrop.role_id,
            };

            // save data
            let rs: any = await this.usersService.save(data);
            console.log("saveUser :",rs);
            if (rs.data.results.length > 0) {
                this.user_id = rs.data.results[0].user_id;
                this.saveProfiles();
            // close form
            this.displayForm = false;

            //refresh data
            await this.getData();

            }

        }
    }

    async update() {
        this.updateUser();
    }

    // function update data profile
    async updateProfile() {
        let id = this.userIdEdit;
        let body = {
            cid: this.cidEdit,
            fullname: this.fullNameEdit,
            hospital_id: this.hospitalId,
            phone_number: this.phoneNumberEdit,
            position: this.posiTionEdit,
            title: this.tiTleEdit,
            service_id: this.serviceId,
        };

        console.log(body);

        // update data
        await this.usersService.updateProfiles(id, body);
    }

    // onchange(data: any) {
    //     console.log(data);

    //     this.selectedDrop = data.role_id;
    // }

    // function update data
    async updateUser() {
        console.log(this.selectedDrop);

        let data: any;
        if (this.passWordEdit) {
            data = {
                username: this.userNameEdit,
                password: this.passWordEdit,
                is_active: this.userActiveEdit,
                role_id: this.selectedDrop.role_id,
            };
            console.log(data);
        } else {
            data = {
                username: this.userNameEdit,
                is_active: this.userActiveEdit,
                role_id: this.selectedDrop.role_id,
            };
            console.log(data);
        }

        // save data
        let rs: any = await this.usersService.update(this.userIdEdit, data);

        this.updateProfile();
        // close form
        this.displayForm = false;

        //refresh data
        await this.getData();
    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false,
        };
        await this.usersService.update(id, body);

        // refresh data
        await this.getData();
    }

    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true,
        };
        await this.usersService.update(id, body);

        // refresh data
        await this.getData();
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image =
            this.layoutService.config.colorScheme === 'dark'
                ? 'line-effect-dark.svg'
                : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }

    async showDropdown(id:any){
        let hospital:any=[];
        this.hospitals=[];
        console.log("showDropdown",id);
        if(id == 3){
            this.is_service = true;
        }else{
            this.is_service = false;
        }
        if(id == 1){
            for(let v of this.hospitalBack){
                if(v.hospital_id == 1){
                    hospital.push(v)
                }
            }
            this.hospitals = hospital
        }else{
            for(let v of this.hospitalBack){
                if(v.hospital_id == 1 || v.hospital_id == 4){
                    // console.log('ww');
                    
                }else{

                    hospital.push(v)
                }
            }
            this.hospitals = hospital
        }
        // this.is_hospital = true;
    }
}
