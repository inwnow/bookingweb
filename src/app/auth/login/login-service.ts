import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // pathPrefix: any = `:40014`
  pathPrefixAuth:any= `:40001`
  


  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixAuth}`
  });


  constructor() {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });
  }

  async login(username: any, password: any) {
    const url = `/login`;
    return this.axiosInstance.post(url, {
      username, password
    });
  }
  async getUserInfo() {
    const url = `/profile/info`;
    return this.axiosInstance.get(url);
  }

}
